import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {createStore} from 'redux';
import {Provider} from 'react-redux';

//Initial State 
// const initialState = {};

//Reducer 
//Utilize switch case to switch between different actions 
// function reducer(state = initialState, action) {
//   return(
//     ...state
//   );
// }

// const store = createStore(reducer);


ReactDOM.render(

  //Include provider with store = {store}
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
