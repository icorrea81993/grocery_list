import React from 'react';

function CostcoList(props){
    let myList = ["Red Bull","Bagels","Cream Cheese","Coca-Cola","Spinach"]

    return(
        <div className="CostcoList">
            {myList.map((list) => <h5 key={list}>{list}</h5>)}
        </div>
    )
}

export default CostcoList; 