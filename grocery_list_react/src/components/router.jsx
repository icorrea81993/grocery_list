import React from 'react';
import {BrowserRouter,Route} from 'react-router-dom';
import CostcoList from './CostcoList';
import JewelList from './JewelList';
import traderJoesList from './traderJoesList';


function ReactRouter() {
    return(
        <React.Fragment>
            <BrowserRouter>
                <Route path="/CostcoItems" component={CostcoList}></Route>
                <Route path="/JewelItems" component={JewelList}></Route>
                <Route path="/TraderJoesItems" component={traderJoesList} ></Route>
            </BrowserRouter>
        </React.Fragment>
    )
}

export default ReactRouter; 