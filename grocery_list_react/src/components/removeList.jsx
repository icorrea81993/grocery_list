import React from 'react';

class RemoveList extends React.Component {
    constructor(props){
        super(props);
        this.state = {store:""}
    }

    formInput(myEvent){
        this.setState({
            [myEvent.target.name]:myEvent.target.value
        });
    }

    // submitForm(event) {
    //     let myObject = {
    //         "store":this.state.store,
    //         "items":this.state.items
    //     }
    // }

    render() {
        return (
            <React.Fragment>
                <form className="removeForm">
                    <input type="text" name="store" value={this.state.store} placeholder="Store" onChange={this.formInput} ></input>
                    <br></br>
                    <button type="submit" className="btn btn-primary">Remove List</button>
                </form>
            </React.Fragment>
        )
    }
}

export default RemoveList;