import React from 'react';

class GroceryList extends React.Component{
    constructor(props){
        super(props);
        this.state = {myList: [{store:"Costco",items:"5"},{store:"Jewel",items:"3"},{store:"Trader Joes",items:"2"}]};
    }

    render() {
        return (
            <React.Fragment>
                <table className="storeTable">
                    <tr>
                        <th>Store Column</th>
                        <th>Items Column</th>
                    </tr>
                    {this.state.myList.map((myObject) => 
                        <tr key={myObject.store}>
                            <td>{myObject.store}</td>
                            <td>{myObject.items}</td>
                        </tr>
                    )}
                </table>
            </React.Fragment>
        )
    }
}

export default GroceryList; 