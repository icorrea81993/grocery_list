import React from 'react';

class AddGroceryItem extends React.Component {
    constructor(props){
        super(props);
        this.state = {store:"",items:""}
    }

    formInput(myEvent){
        this.setState({
            [myEvent.target.name]:myEvent.target.value
        });
    }

    // submitForm(event) {
    //     let myObject = {
    //         "store":this.state.store,
    //         "items":this.state.items
    //     }
    // }

    render() {
        return (
            <React.Fragment>
                <form className="groceryItemForm">
                    <input type="text" name="store" value={this.state.store} placeholder="Store" onChange={this.formInput} ></input>
                    <br></br>
                    <input type="text" name="items" value={this.state.items} placeholder="Item" onChange={this.formInput}></input>
                    <br></br>
                    <button type="submit" className="btn btn-primary">Add Item</button>
                </form>
            </React.Fragment>
        )
    }
}

export default AddGroceryItem;