import React from 'react';

class RemoveItem extends React.Component {
    constructor(props){
        super(props);
        this.state = {item:""}
    }

    formInput(myEvent){
        this.setState({
            [myEvent.target.name]:myEvent.target.value
        });
    }

    // submitForm(event) {
    //     let myObject = {
    //         "store":this.state.store,
    //         "items":this.state.items
    //     }
    // }

    render() {
        return (
            <React.Fragment>
                <form className="removeItemForm">
                    <input type="text" name="item" value={this.state.store} placeholder="Item" onChange={this.formInput} ></input>
                    <br></br>
                    <button type="submit" className="btn btn-primary">Remove Item</button>
                </form>
            </React.Fragment>
        )
    }
}

export default RemoveItem;