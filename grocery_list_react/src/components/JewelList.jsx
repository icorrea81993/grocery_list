import React from 'react';

function JewelList(props){
    let myList = ["Whiskey","Vodka","Corona Light"]

    return(
        <div className="JewelList">
            {myList.map((list) => <h5 key={list}>{list}</h5>)}
        </div>
    )
}

export default JewelList; 