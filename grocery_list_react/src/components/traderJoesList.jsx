import React from 'react';

function traderJoesList(props){
    let myList = ["Orange Juice","Apple Juice"]

    return(
        <div className="traderJoesList">
            {myList.map((list) => <h5 key={list}>{list}</h5>)}
        </div>
    )
}

export default traderJoesList; 