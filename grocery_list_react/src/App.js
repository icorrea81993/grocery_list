import React from 'react';
import logo from './logo.svg';
import './App.css';
import GroceryList from './components/groceryLists';
import AddGroceryList from './components/addGroceryListForm';
import UpdateGroceryList from './components/updateForm';
import RemoveList from './components/removeList';
import ReactRouter from './components/router';
import AddGroceryItem from './components/groceryItemForm';
import UpdateGroceryItemList from './components/updateGroceryItem';
import RemoveItem from './components/removeItem';
import {connect} from 'react-redux';

function App() {
  return (
    <div> 
      <h3 className="header"><a href="http://localhost:3000">Grocery Lists</a></h3>
      <GroceryList></GroceryList>
      <AddGroceryList></AddGroceryList>
      <UpdateGroceryList></UpdateGroceryList>
      <RemoveList></RemoveList>
      <ReactRouter></ReactRouter>
      <a href="http://localhost:3000/CostcoItems" className="costcoLink">Costco Items</a>
      <a href="http://localhost:3000/JewelItems" className="jewelLink">Jewel Items</a>
      <a href="http://localhost:3000/TraderJoesItems" className="traderJoesLink">Trader Joes Items</a>
      <AddGroceryItem></AddGroceryItem>
      <UpdateGroceryItemList></UpdateGroceryItemList>
      <RemoveItem></RemoveItem>
    </div>
  );
}

// function mapStateToProps(state){
//   return {
    
//   }
// }

//utilize connect and map dispatch to props 
export default App;
