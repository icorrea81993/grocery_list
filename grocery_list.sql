CREATE TABLE GroceryList(
	groceryList_Id NUMERIC PRIMARY KEY,
	list_name VARCHAR(50) NOT NULL
)

DROP TABLE GroceryList; 

CREATE TABLE GroceryItem(
	groceryList_Id NUMERIC PRIMARY KEY,
	grocery_item VARCHAR(50) NOT NULL,
	groceryListIdentifier NUMERIC NOT NULL,
	FOREIGN KEY (groceryListIdentifier) REFERENCES GroceryList (groceryList_Id)
)

DROP TABLE GroceryItem; 

CREATE TABLE Grocery_Type(
	groceryType_Id NUMERIC PRIMARY KEY,
	food VARCHAR(50), 
	electronics VARCHAR(50),
	hygiene VARCHAR(50)
)

DROP TABLE Grocery_Type;

INSERT INTO GroceryList VALUES (1,'Costco List');
INSERT INTO GroceryList VALUES (2,'Jewel List');
INSERT INTO GroceryList VALUES (3,'Trader Joes List');

INSERT INTO GroceryItem VALUES (1,'Red Bull',1);
INSERT INTO GroceryItem VALUES (2,'Bagels',2);
INSERT INTO GroceryItem VALUES (3,'Orange Juice',3);