package com.example.grocerylist.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.grocerylist.model.GroceryItem;
import com.example.grocerylist.model.GroceryList;
import com.example.grocerylist.repository.GroceryListRepo;

@RestController
public class GroceryListController {

	private GroceryListRepo groceryListRepo;
	
	public GroceryListController() {
		
	}
	
	
	@Autowired
	public GroceryListController(GroceryListRepo groceryListRepo) {
		super();
		this.groceryListRepo = groceryListRepo;
	}



	@GetMapping(value="/grocery-list")
	public List<GroceryList> findAllGroceryLists() {
		return groceryListRepo.findAll();
	}
	
	@GetMapping(value="/grocery-list/{listId}/item")
	public ResponseEntity<GroceryList> getListById(@PathVariable("id")int id){
		return null;
	}
	
	@PostMapping(value="/grocery-list")
	public String createNewList(@RequestBody GroceryList newList) {
		return null;
	}
	
	@PostMapping(value="/grocery-list/{listId}")
	public String createNewItem(@RequestBody GroceryItem newItem, @PathVariable("id") int id ) {
		return null;
	}
	
	@PutMapping(value="/grocery-list/{listId}/item")
	public String updateList(@RequestBody GroceryList updateList, @PathVariable("id") int id) {
		return null; 
	}
	
	@PutMapping(value="/grocery-list/{listId}/item/{itemId}")
	public String updateItem(@RequestBody GroceryItem updateItem, @PathVariable("id") int id) {
		return null;
	}

	@DeleteMapping(value="/grocery-list/{listId}")
	public boolean deleteList(@RequestBody GroceryList deleteList, @PathVariable("id") int id) {
		return true; 
	}
	
	@DeleteMapping(value="/grocery-list/{listId}/item/{itemId}")
	public boolean deleteItem(@RequestBody GroceryItem deleteItem , @PathVariable("id") int id) {
		return true; 
	}
}
