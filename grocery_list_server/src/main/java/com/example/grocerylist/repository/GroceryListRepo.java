package com.example.grocerylist.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.grocerylist.model.GroceryList;

public interface GroceryListRepo extends CrudRepository<GroceryList,Integer> {

	public List<GroceryList> findAll();
	
}
