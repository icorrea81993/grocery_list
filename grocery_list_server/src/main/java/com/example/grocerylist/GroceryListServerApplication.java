package com.example.grocerylist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@EnableAutoConfiguration 
public class GroceryListServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GroceryListServerApplication.class, args);
	}

}
