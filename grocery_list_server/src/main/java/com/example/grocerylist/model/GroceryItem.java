package com.example.grocerylist.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity 
@Table(name="grocery_item")
public class GroceryItem {
	
	@Id 
	@Column(name="grocery_item_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int groceryItemId;
	
	@Column(name="grocery_item")
	private String groceryItem;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="grocery_list_id")
	private GroceryList groceryListIdentifier;

	public GroceryItem() {
	
	}

	public GroceryItem(int groceryItemId, String groceryItem, GroceryList groceryListIdentifier) {
		super();
		this.groceryItemId = groceryItemId;
		this.groceryItem = groceryItem;
		this.groceryListIdentifier = groceryListIdentifier;
	}

	public GroceryItem(String groceryItem, GroceryList groceryListIdentifier) {
		super();
		this.groceryItem = groceryItem;
		this.groceryListIdentifier = groceryListIdentifier;
	}

	public int getGroceryItemId() {
		return groceryItemId;
	}

	public void setGroceryItemId(int groceryItemId) {
		this.groceryItemId = groceryItemId;
	}

	public String getGroceryItem() {
		return groceryItem;
	}

	public void setGroceryItem(String groceryItem) {
		this.groceryItem = groceryItem;
	}

	public GroceryList getGroceryListIdentifier() {
		return groceryListIdentifier;
	}

	public void setGroceryListIdentifier(GroceryList groceryListIdentifier) {
		this.groceryListIdentifier = groceryListIdentifier;
	}

	@Override
	public String toString() {
		return "GroceryItem [groceryItemId=" + groceryItemId + ", groceryItem=" + groceryItem
				+ ", groceryListIdentifier=" + groceryListIdentifier + "]";
	}
	
	
	
	
}
