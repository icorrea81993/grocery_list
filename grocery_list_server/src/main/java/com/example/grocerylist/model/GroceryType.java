package com.example.grocerylist.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Grocery_Type")
public class GroceryType {
	
	@Id 
	@Column(name="groceryType_Id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int groceryTypeId;
	
	@Column(name="food")
	private String food;
	
	@Column(name="electronics")
	private String electronics;
	
	@Column(name="hygiene")
	private String hygiene; 
	
	public GroceryType() {
		
	}

	public GroceryType(int groceryTypeId, String food, String electronics, String hygiene) {
		super();
		this.groceryTypeId = groceryTypeId;
		this.food = food;
		this.electronics = electronics;
		this.hygiene = hygiene;
	}

	public GroceryType(String food, String electronics, String hygiene) {
		super();
		this.food = food;
		this.electronics = electronics;
		this.hygiene = hygiene;
	}

	public int getGroceryTypeId() {
		return groceryTypeId;
	}

	public void setGroceryTypeId(int groceryTypeId) {
		this.groceryTypeId = groceryTypeId;
	}

	public String getFood() {
		return food;
	}

	public void setFood(String food) {
		this.food = food;
	}

	public String getElectronics() {
		return electronics;
	}

	public void setElectronics(String electronics) {
		this.electronics = electronics;
	}

	public String getHygiene() {
		return hygiene;
	}

	public void setHygiene(String hygiene) {
		this.hygiene = hygiene;
	}

	@Override
	public String toString() {
		return "GroceryType [groceryTypeId=" + groceryTypeId + ", food=" + food + ", electronics=" + electronics
				+ ", hygiene=" + hygiene + "]";
	}
}
