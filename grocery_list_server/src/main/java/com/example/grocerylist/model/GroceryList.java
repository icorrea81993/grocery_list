package com.example.grocerylist.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table(name="grocery_list")
public class GroceryList {

	@Id 
	@Column(name="grocery_list_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int groceryListId; 
	
	@Column(name="list_name")
	private String listName; 
	
	public GroceryList() {
		
	}

	public GroceryList(int groceryListId, String listName) {
		super();
		this.groceryListId = groceryListId;
		this.listName = listName;
	}

	public GroceryList(String listName) {
		super();
		this.listName = listName;
	}

	public int getGroceryListId() {
		return groceryListId;
	}

	public void setGroceryListId(int groceryListId) {
		this.groceryListId = groceryListId;
	}

	public String getListName() {
		return listName;
	}

	public void setListName(String listName) {
		this.listName = listName;
	}

	@Override
	public String toString() {
		return "GroceryList [groceryListId=" + groceryListId + ", listName=" + listName + "]";
	}
	
	
}
